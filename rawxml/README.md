<!-- vim: set tw=78 sts=2 ai expandtab: -->
# Updating English contents of debian-reference

2024-01-28T21:12:00 UTC

Osamu Aoki, <osamu at debian dot org>

## Is there easier way to edit XML source

Unfortunately, no.

## Reminders for contents

Let's ask ourselves several questions before adding any contents.

* Do they qualify as the minimum set of information which new Debian users
  should be exposed?
* Are they expressed in the shortest possible way?
* Can we use common text in table to reduce translation efforts?
* Are they actively used in the Debian infrastructure or in the popular
  sub-project such as freedombox, debian-edu, ...?
* Do I personally use them regularly (or previously)?
* Have I personally tested or inspected key features of programs to be
  mentioned in them, at least?
* Are they about actively maintained program? (No sourceforge.net upstream.)
* Are they the best/better program offering the feature?
* Are they about main stream program?
  * POPCON V>100 : popular and OK to add
  * POPCON I>5  : OK to add if you used or tested it and found useful
  * POPCON I<5   : Don't add unless you have strong arguments
* Are they about main stream working environment? (GNOME?)
* Is there any strong reason to add them?
  * sysv to systemd transition
  * UEFI transition
  * OAUTH2 support for SMTP/IMAP4 (future TODO)
  * lxd to incus transition (future TODO)
  * GNOME45 transition (future TODO)

## Reminders for commit

Split commits as small change set as possible.

Automated conversion such as "make po" and "make entity" should be a separate
commit to make it easy to revert.

Osamu

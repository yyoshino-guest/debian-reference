#! /bin/sh -e

# vim:set sw=2 et sts=2:
TITLE="Debian Reference (version 2)"
PACKAGE="debian-reference" # Yes this has been changed
DR_DOC_ROOT=${DR_DOC_ROOT:-/usr/share/doc/debian-reference-common}
README="/usr/share/doc/debian-reference-common/README"

# Echo Language name
echolang () {
  case "$1" in
    "en") echo "<dt><b>English</b></dt>";;
    "fr") echo "<dt><b>French</b></dt>";;
    "it") echo "<dt><b>Italian</b></dt>";;
    "es") echo "<dt><b>Spanish</b></dt>";;
    "de") echo "<dt><b>German</b></dt>";;
    "pl") echo "<dt><b>Polish</b></dt>";;
    "pt") echo "<dt><b>Portuguese (Portugal)</b></dt>";;
    "pt-br") echo "<dt><b>Portuguese (Brazil)</b></dt>";;
    "zh-tw") echo "<dt><b>Chinese (Traditional)</b></dt>";;
    "zh-cn") echo "<dt><b>Chinese (Simplified)</b></dt>";;
    "ja") echo "<dt><b>Japanese</b></dt>";;
    "id") echo "<dt><b>Indonesian</b></dt>";;
    *) echo "<dt>$1</dt>";;
  esac
}

# Echo index page
echoindex () {
  echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\">"
  echo "<html>"
  echo "<head>"
  echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">"
  echo "<title>${TITLE}</title>"
  echo "</head>"
  echo "<body>"
  echo "<hr>"
  echo "<h1>${TITLE}</h1>"
  if [ -n "${LANGS}" ]; then
    echo "<hr>"
    echo "<p>"
    echo "<dl>"
    for lang in ${LANGS}; do
      echolang "$lang"
      echo "<dd>"
      echo "<a href=\"index.$lang.html\">HTML (multi files)</a>"
      if [ -f "$PACKAGE.$lang.html" ]; then
        echo ", <a href=\"$PACKAGE.$lang.html\">HTML (single file)</a>"
      fi
      if [ -f "$PACKAGE.$lang.txt.gz" ]; then
        echo ", <a href=\"$PACKAGE.$lang.txt.gz\">UTF-8 plain text (gzipped)</a>"
      elif [ -f "$PACKAGE.$lang.txt" ]; then
        echo ", <a href=\"$PACKAGE.$lang.txt\">UTF-8 plain text</a>"
      fi
      if [ -f "$PACKAGE.$lang.epub" ]; then
        echo ", <a href=\"$PACKAGE.$lang.epub\">epub</a>"
      fi
      #    echo ", <a href=\"$PACKAGE.$lang.ps.gz\">PS</a>"
      echo ", <a href=\"$PACKAGE.$lang.pdf\">PDF</a>"
      echo "</dd>"
    done
    echo "</dl>"
    echo "</p>"
  fi
  echo "<hr>"
  echo "<p>All files are encoded in <b>UTF-8</b>.</p>"
  echo "<p>If your favorite language is not found in this list, please install the corresponding <tt>debian-reference-*</tt> package.</p>"
  echo "<p>Some browsers may not be setup to read compressed plain text. Use appropriate file viewer system to read them directly from <a href=\"$DR_DOC_ROOT/docs\">$DR_DOC_ROOT/docs</a>. See <a href=\"$README\">README<a> for more help and information.</p>"
  echo "</body>"
  echo "</html>"
}

# Echo index page (No language found)
echoindex0 () {
  echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\">"
  echo "<html>"
  echo "<head>"
  echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">"
  echo "<title>${TITLE}</title>"
  echo "</head>"
  echo "<body>"
  echo "<hr>"
  echo "<h1>${TITLE}</h1>"
  echo "<hr>"
  echo "<p>Please install document content packages to create meaningful links to be displayed in the browser.</p>"
  echo "<dl>"
  echo "<dd>'debian-reference' for all languages.</dd>"
  echo "<dd>'debian-reference-en' for English.</dd>"
  echo "<dd>'debian-reference-*' for each other language.</dd>"
  echo "</dl>"
  echo "<hr>"
  echo "<p>All files are encoded in <b>UTF-8</b>.</p>"
  echo "<p>Some browsers may not be setup to read compressed plain text. Use appropriate file viewer system to read them directly from <a href=\"$DR_DOC_ROOT/docs\">$DR_DOC_ROOT/docs</a>. See <a href=\"$README\">README<a> for more help and information.</p>"
  echo "</body>"
  echo "</html>"
}
# Skip if $DR_DOC_ROOT directory or $DR_DOC_ROOT/docs are missing
# Always remove index page and move to the $DR_DOC_ROOT directory
if [ -d "$DR_DOC_ROOT" ] && [ -d "$DR_DOC_ROOT/docs" ]; then
  cd "$DR_DOC_ROOT/docs"
  # index.html can be symlink or file
  rm -f index.html
else
  # You may have removed the $DR_DOC_ROOT directory
  exit 0
fi
# Always sort language with English as top.
# shellcheck disable=SC2012
LANGS="$(ls -1 --color=never index.*.html 2> /dev/null | \
  sed 's/index\.//g;s/\.html//g;s/en/00/' | \
  sort | sed 's/00/en/')"

HTML_INSTALLED=$(echo "${LANGS}" | wc -w)

# Create single index page for menu:
if [ "$HTML_INSTALLED" = 0 ]; then
  # remove index file only
  # keep images/ and debian-reference.css
  echoindex0 > index.html
else
  echoindex > index.html
fi

exit 0
